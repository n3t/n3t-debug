<?xml version="1.0" encoding="utf-8"?>
<extension type="plugin" version="3.4.0" group="system" method="upgrade">
  <name>plg_system_n3tdebug</name>
  <creationDate>2015-05-02</creationDate>
  <author>Pavel Poles - n3t.cz</author>
  <authorUrl>n3t.bitbucket.io</authorUrl>
  <copyright>(c) 2016-2024 Pavel Poles - n3t.cz. All rights reserved.</copyright>
  <license>GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html</license>
  <version>5.0.3</version>
  <description>PLG_SYSTEM_N3TDEBUG_MANIFEST_DESCRIPTION</description>

  <help url="PLG_SYSTEM_N3TDEBUG_MANIFEST_HELP_URL" />

  <files>
    <folder>editor</folder>
    <folder>fields</folder>
    <folder>helpers</folder>
    <folder>n3tDebug</folder>

    <filename plugin="n3tdebug">n3tdebug.php</filename>
  </files>

  <languages folder="language">
    <language tag="en-GB">en-GB/en-GB.plg_system_n3tdebug.ini</language>
    <language tag="en-GB">en-GB/en-GB.plg_system_n3tdebug.sys.ini</language>
  </languages>

  <config>
    <fields name="params" addfieldpath="/plugins/system/n3tdebug/fields">
      <fieldset name="basic">
        <field name="version" type="n3tdebugversion" label="PLG_SYSTEM_N3TDEBUG_CFG_VERSION" />
        <field name="version_tracy" type="tracyversion" label="PLG_SYSTEM_N3TDEBUG_CFG_TRACY_VERSION" />
      </fieldset>

      <fieldset name="debugger" label="PLG_SYSTEM_N3TDEBUG_CFG_DEBUGGER_SETTINGS" description="">
        <field name="enabled_site" type="radio" default="1" class="btn-group btn-group-yesno" layout="joomla.form.field.radio.switcher" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_ENABLED_SITE" description="PLG_SYSTEM_N3TDEBUG_CFG_ENABLED_SITE_DESC">
          <option value="0">JNO</option>
          <option value="1">JYES</option>
        </field>
        <field name="enabled_admin" type="radio" default="1" class="btn-group btn-group-yesno" layout="joomla.form.field.radio.switcher" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_ENABLED_ADMIN" description="PLG_SYSTEM_N3TDEBUG_CFG_ENABLED_ADMIN_DESC">
          <option value="0">JNO</option>
          <option value="1">JYES</option>
        </field>
        <field name="ip_list" type="iplist" default="" label="PLG_SYSTEM_N3TDEBUG_CFG_IP_LIST" description="PLG_SYSTEM_N3TDEBUG_CFG_IP_LIST_DESC" rows="10" cols="35" />
        <field name="dump_theme" type="list" default="light" label="PLG_SYSTEM_N3TDEBUG_CFG_DUMP_THEME" description="PLG_SYSTEM_N3TDEBUG_CFG_DUMP_THEME_DESC">
          <option value="light">PLG_SYSTEM_N3TDEBUG_CFG_DUMP_THEME_LIGHT</option>
          <option value="dark">PLG_SYSTEM_N3TDEBUG_CFG_DUMP_THEME_DARK</option>
        </field>
        <field name="dump_max_length" type="number" default="150" min="0" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_DUMP_MAX_LENGTH" description="PLG_SYSTEM_N3TDEBUG_CFG_DUMP_MAX_LENGTH_DESC" />
        <field name="dump_max_depth" type="number" default="15" min="1" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_DUMP_MAX_DEPTH" description="PLG_SYSTEM_N3TDEBUG_CFG_DUMP_MAX_DEPTH_DESC" />
        <field name="dump_keys_to_hide" type="subform" layout="joomla.form.field.subform.repeatable-table" multiple="true" label="PLG_SYSTEM_N3TDEBUG_CFG_DUMP_KEYS_TO_HIDE" description="PLG_SYSTEM_N3TDEBUG_CFG_DUMP_KEYS_TO_HIDE_DESC">
          <form>
            <field name="name" type="text" required="required" label="PLG_SYSTEM_N3TDEBUG_CFG_DUMP_KEYS_TO_HIDE_NAME" class="d-block w-100 input-block-level" />
          </form>
        </field>
        <field name="ignore_404" type="radio" default="1" class="btn-group btn-group-yesno" layout="joomla.form.field.radio.switcher" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_IGNORE_404" description="PLG_SYSTEM_N3TDEBUG_CFG_IGNORE_404_DESC">
          <option value="0">JNO</option>
          <option value="1">JYES</option>
        </field>
        <field name="ignore_levels" type="list" multiple="true" default="[0]" layout="joomla.form.field.list-fancy-select" label="PLG_SYSTEM_N3TDEBUG_CFG_IGNORE_LEVELS" description="PLG_SYSTEM_N3TDEBUG_CFG_IGNORE_LEVELS_DESC">
          <option value="1">E_ERROR</option>
          <option value="2">E_WARNING</option>
          <option value="4">E_PARSE</option>
          <option value="8">E_NOTICE</option>
          <option value="16">E_CORE_ERROR</option>
          <option value="32">E_CORE_WARNING</option>
          <option value="64">E_COMPILE_ERROR</option>
          <option value="128">E_COMPILE_WARNING</option>
          <option value="256">E_USER_ERROR</option>
          <option value="512">E_USER_WARNING</option>
          <option value="1024">E_USER_NOTICE</option>
          <option value="2048">E_STRICT</option>
          <option value="4096">E_RECOVERABLE_ERROR</option>
          <option value="8192">E_DEPRECATED</option>
          <option value="16384">E_USER_DEPRECATED</option>
          <option value="0">E_NONE</option>
        </field>
        <field name="strict_mode" type="radio" default="0" class="btn-group btn-group-yesno" layout="joomla.form.field.radio.switcher" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_STRICT_MODE" description="PLG_SYSTEM_N3TDEBUG_CFG_STRICT_MODE_DESC">
          <option value="0">JNO</option>
          <option value="1">JYES</option>
        </field>
        <field name="strict_mode_ignore_levels" type="list" showon="strict_mode:1" multiple="true" default="[8192,16384]" layout="joomla.form.field.list-fancy-select" label="PLG_SYSTEM_N3TDEBUG_CFG_STRICT_MODE_IGNORE_LEVELS" description="PLG_SYSTEM_N3TDEBUG_CFG_STRICT_MODE_IGNORE_LEVELS_DESC">
          <option value="1">E_ERROR</option>
          <option value="2">E_WARNING</option>
          <option value="4">E_PARSE</option>
          <option value="8">E_NOTICE</option>
          <option value="16">E_CORE_ERROR</option>
          <option value="32">E_CORE_WARNING</option>
          <option value="64">E_COMPILE_ERROR</option>
          <option value="128">E_COMPILE_WARNING</option>
          <option value="256">E_USER_ERROR</option>
          <option value="512">E_USER_WARNING</option>
          <option value="1024">E_USER_NOTICE</option>
          <option value="2048">E_STRICT</option>
          <option value="4096">E_RECOVERABLE_ERROR</option>
          <option value="8192">E_DEPRECATED</option>
          <option value="16384">E_USER_DEPRECATED</option>
          <option value="0">E_NONE</option>
        </field>
        <field name="scream" type="radio" default="0" class="btn-group btn-group-yesno" layout="joomla.form.field.radio.switcher" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_SCREAM" description="PLG_SYSTEM_N3TDEBUG_CFG_SCREAM_DESC">
          <option value="0">JNO</option>
          <option value="1">JYES</option>
        </field>
      </fieldset>

      <fieldset name="panels" label="PLG_SYSTEM_N3TDEBUG_CFG_PANELS" description="">
        <field name="panels_joomla" type="note" label="PLG_SYSTEM_N3TDEBUG_CFG_PANELS_JOOMLA" />
        <field name="panel_jinfo" type="radio" default="1" class="btn-group btn-group-yesno" layout="joomla.form.field.radio.switcher" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_JINFO" description="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_JINFO_DESC">
          <option value="0">JNO</option>
          <option value="1">JYES</option>
        </field>
        <field name="panel_jprofile" type="radio" default="1" class="btn-group btn-group-yesno" layout="joomla.form.field.radio.switcher" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_JPROFILE" description="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_JPROFILE_DESC">
          <option value="0">JNO</option>
          <option value="1">JYES</option>
        </field>
        <field name="panel_juser" type="radio" default="1" class="btn-group btn-group-yesno" layout="joomla.form.field.radio.switcher" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_JUSER" description="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_JUSER_DESC">
          <option value="0">JNO</option>
          <option value="1">JYES</option>
        </field>
        <field name="panel_jlanguage" type="radio" default="1" class="btn-group btn-group-yesno" layout="joomla.form.field.radio.switcher" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_JLANGUAGE" description="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_JLANGUAGE_DESC">
          <option value="0">JNO</option>
          <option value="1">JYES</option>
        </field>
        <field name="panel_jlog" type="radio" default="1" class="btn-group btn-group-yesno" layout="joomla.form.field.radio.switcher" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_JLOG" description="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_JLOG_DESC">
          <option value="0">JNO</option>
          <option value="1">JYES</option>
        </field>
        <field name="panel_jsession" type="radio" default="1" class="btn-group btn-group-yesno" layout="joomla.form.field.radio.switcher" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_JSESSION" description="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_JSESSION_DESC">
          <option value="0">JNO</option>
          <option value="1">JYES</option>
        </field>
        <field name="panel_jmail" type="radio" default="1" class="btn-group btn-group-yesno" layout="joomla.form.field.radio.switcher" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_JMAIL" description="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_JMAIL_DESC">
          <option value="0">JNO</option>
          <option value="1">JYES</option>
        </field>
        <field name="panel_jscripts" type="radio" default="1" class="btn-group btn-group-yesno" layout="joomla.form.field.radio.switcher" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_JSCRIPTS" description="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_JSCRIPTS_DESC">
          <option value="0">JNO</option>
          <option value="1">JYES</option>
        </field>
        <field name="panel_jstylesheets" type="radio" default="1" class="btn-group btn-group-yesno" layout="joomla.form.field.radio.switcher" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_JSTYLESHEETS" description="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_JSTYLESHEETS_DESC">
          <option value="0">JNO</option>
          <option value="1">JYES</option>
        </field>
        <field name="panel_jdatabase" type="radio" default="1" class="btn-group btn-group-yesno" layout="joomla.form.field.radio.switcher" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_JDATABASE" description="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_JDATABASE_DESC">
          <option value="0">JNO</option>
          <option value="1">JYES</option>
        </field>

        <field name="panels_php" type="note" label="PLG_SYSTEM_N3TDEBUG_CFG_PANELS_PHP" />
        <field name="panel_server" type="radio" default="1" class="btn-group btn-group-yesno" layout="joomla.form.field.radio.switcher" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_SERVER" description="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_SERVER_DESC">
          <option value="0">JNO</option>
          <option value="1">JYES</option>
        </field>
        <field name="panel_request" type="radio" default="1" class="btn-group btn-group-yesno" layout="joomla.form.field.radio.switcher" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_REQUEST" description="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_REQUEST_DESC">
          <option value="0">JNO</option>
          <option value="1">JYES</option>
        </field>
        <field name="panel_get" type="radio" default="1" class="btn-group btn-group-yesno" layout="joomla.form.field.radio.switcher" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_GET" description="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_GET_DESC">
          <option value="0">JNO</option>
          <option value="1">JYES</option>
        </field>
        <field name="panel_post" type="radio" default="1" class="btn-group btn-group-yesno" layout="joomla.form.field.radio.switcher" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_POST" description="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_POST_DESC">
          <option value="0">JNO</option>
          <option value="1">JYES</option>
        </field>
        <field name="panel_cookie" type="radio" default="1" class="btn-group btn-group-yesno" layout="joomla.form.field.radio.switcher" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_COOKIE" description="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_COOKIE_DESC">
          <option value="0">JNO</option>
          <option value="1">JYES</option>
        </field>
        <field name="panel_session" type="radio" default="1" class="btn-group btn-group-yesno" layout="joomla.form.field.radio.switcher" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_SESSION" description="PLG_SYSTEM_N3TDEBUG_CFG_PANEL_SESSION_DESC">
          <option value="0">JNO</option>
          <option value="1">JYES</option>
        </field>
      </fieldset>

      <fieldset name="advanced" label="PLG_SYSTEM_N3TDEBUG_CFG_ADVANCED" description="">
        <field name="advanced_language" type="note" showon="panel_jlanguage:1" label="PLG_SYSTEM_N3TDEBUG_CFG_ADVANCED_LANGUAGE" />
        <field name="language_untranslated" type="radio" showon="panel_jlanguage:1" default="0" class="btn-group btn-group-yesno" layout="joomla.form.field.radio.switcher" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_LANGUAGE_UNTRANSLATED" description="PLG_SYSTEM_N3TDEBUG_CFG_LANGUAGE_UNTRANSLATED_DESC">
          <option value="0">JNO</option>
          <option value="1">JYES</option>
        </field>
        <field name="advanced_database" type="note" showon="panel_jdatabase:1" label="PLG_SYSTEM_N3TDEBUG_CFG_ADVANCED_DATABASE" />
        <field name="database_explain" type="radio" showon="panel_jdatabase:1" default="0" class="btn-group btn-group-yesno" layout="joomla.form.field.radio.switcher" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_DATABASE_EXPLAIN" description="PLG_SYSTEM_N3TDEBUG_CFG_DATABASE_EXPLAIN_DESC">
          <option value="0">JNO</option>
          <option value="1">JYES</option>
        </field>
        <field name="database_profile" type="radio" showon="panel_jdatabase:1" default="0" class="btn-group btn-group-yesno" layout="joomla.form.field.radio.switcher" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_DATABASE_PROFILE" description="PLG_SYSTEM_N3TDEBUG_CFG_DATABASE_PROFILE_DESC">
          <option value="0">JNO</option>
          <option value="1">JYES</option>
        </field>
        <field name="database_callstack" type="radio" showon="panel_jdatabase:1" default="0" class="btn-group btn-group-yesno" layout="joomla.form.field.radio.switcher" filter="integer" label="PLG_SYSTEM_N3TDEBUG_CFG_DATABASE_CALLSTACK" description="PLG_SYSTEM_N3TDEBUG_CFG_DATABASE_CALLSTACK_DESC">
          <option value="0">JNO</option>
          <option value="1">JYES</option>
        </field>
        <field name="database_max_queries" type="text" showon="panel_jdatabase:1" default="200" class="input-small" label="PLG_SYSTEM_N3TDEBUG_CFG_DATABASE_MAX_QUERIES" description="PLG_SYSTEM_N3TDEBUG_CFG_DATABASE_MAX_QUERIES_DESC" filter="int" />
      </fieldset>
    </fields>
  </config>
</extension>
