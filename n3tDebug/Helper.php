<?php
/**
 * @package n3t Debug
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2016-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

namespace n3tDebug;

defined( '_JEXEC' ) or die;

use Joomla\Utilities\IpHelper;

class Helper
{
  public const COLOR_INFO = '#5091cd';
  public const COLOR_SUCCESS = '#7ac143';
  public const COLOR_WARNING = '#f9a541';
  public const COLOR_ERROR = '#f44321';
  public const COLOR_GRAY = '#999999';

  public static function isLocalhost(): bool
  {
    return IpHelper::IPinList(IpHelper::getIp(), [
      '127.0.0.1',
      '::1',
    ]);
  }

  public static function fileLink(string $file, $lines = 0, string $action = 'open'): string
  {
    $displayName = str_replace(JPATH_ROOT . '/', '', $file);
    $displayName = str_replace(JPATH_ROOT . '\\', '', $displayName);
    $displayLine = '';

    if (is_array($lines)) {
      $line = $lines[0];
      $displayLine = ': ' . implode(', ', $lines);
    } else {
      $line = $lines;
      if ($line)
        $displayLine = ': ' . $line;
    }

    if (self::isLocalhost())
      return '<a href="editor://' . $action . '/?file=' . urlencode($file) . '&line=' . $line . '">' . $displayName . '</a>' . $displayLine;
    else
      return $displayName . $displayLine;
  }

}
