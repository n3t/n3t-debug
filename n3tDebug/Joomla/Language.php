<?php
/**
 * @package n3t Debug
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2016-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

namespace n3tDebug\Joomla;

defined( '_JEXEC' ) or die;

use Tracy\Debugger;

defined( '_JEXEC' ) or die( 'Restricted access' );

class Language extends \Joomla\CMS\Language\Language {

  private $ignoreClassList = [
    'Joomla\\CMS\\Language\\Language',
    '\\Joomla\\CMS\\Language\\Language',
    'JText',
    '\\JText',
    'Joomla\\CMS\\Language\\Text',
    '\\Joomla\\CMS\\Language\\Text',
    'n3tDebug\\Joomla\\Language',
    '\\n3tDebug\\Joomla\\Language',
  ];

  private $debugLoadFallback = false;

  public function __construct($lang = null, $debug = false)
  {
    $this->debugLoadFallback = !$debug;

    parent::__construct($lang, true);
  }

  public function _($string, $jsSafe = false, $interpretBackSlashes = true)
  {
    $result = parent::_($string, $jsSafe, $interpretBackSlashes);

    if ($this->debug) {
      $result = preg_replace('~^\?\?(.*)\?\?$~', '$1', $result);
      $result = preg_replace('~^\*\*(.*)\*\*$~', '$1', $result);
    }

    return $result;
  }

  public function reload($extensions)
  {
    foreach ($extensions as $extension => $paths)
      foreach ($paths as $path => $loaded)
        $this->loadLanguage($path, $extension);
  }

  public function load($extension = 'joomla', $basePath = JPATH_BASE, $lang = null, $reload = false, $default = true)
  {
    if ($this->debugLoadFallback)
      $this->debug = false;
    try {
      return parent::load($extension, $basePath, $lang, $reload, $default);
    } finally {
      $this->debug = true;
    }
  }

  protected function getCallerInfo()
  {
    if (!function_exists('debug_backtrace'))
      return [];

    $backtrace = debug_backtrace();
    $info = [];

    $continue = true;

    while ($continue && next($backtrace))
    {
      $step = current($backtrace);
      $class = $step['class'] ?? '';

      if ($class && !in_array($class, $this->ignoreClassList)) {
        $step = prev($backtrace);
        $info['function'] = @$step['function'];
        $info['class'] = $class;
        $info['step'] = $step;

        $info['file'] = @$step['file'];
        $info['line'] = @$step['line'];

        $continue = false;
      }
    }

    return $info;
  }
}
