<?php
/**
 * @package n3t Debug
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2016-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

namespace n3tDebug\Joomla;

defined( '_JEXEC' ) or die;

use Joomla\CMS\Log\Log;

class Mail extends \Joomla\CMS\Mail\Mail
{
  private static $log = [];
  private $errorLog = [];

  public function __construct($exceptions = true)
  {
    parent::__construct($exceptions);

    $this->SMTPDebug = 4;
    $this->Debugoutput = [$this, 'onMailError'];
  }

  public function onMailError($message, $level)
  {
    $this->errorLog[] = $message;
    Log::add(sprintf('Error in Mail API: %s', $message), Log::ERROR, 'mail');
  }

  public function Send()
  {
    $result = parent::Send();

    self::$log[] = $this;

    return $result;
  }

  public static function getInstance($id = 'Joomla', $exceptions = true)
  {
    if (empty(self::$instances[$id]))
      self::$instances[$id] = new Mail($exceptions);

    return parent::getInstance($id, $exceptions);
  }

  public static function getInstances(): array
  {
    return self::$instances;
  }

  public static function getLog(): array
  {
    return self::$log;
  }
}
