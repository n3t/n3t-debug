<?php
/**
 * @package n3t Debug
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2016-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

namespace n3tDebug\Joomla;

defined( '_JEXEC' ) or die;

class Profiler extends \Joomla\CMS\Profiler\Profiler
{

  public static function getInstances(): array
  {
    return self::$instances;
  }

  public static function exists(string $prefix): bool
  {
    return array_key_exists($prefix, self::$instances);
  }
}
