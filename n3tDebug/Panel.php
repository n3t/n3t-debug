<?php
/**
 * @package n3t Debug
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2016-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

namespace n3tDebug;

defined( '_JEXEC' ) or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Version;
use Joomla\Registry\Registry;

abstract class Panel implements \Tracy\IBarPanel
{
  protected $data = null;
  protected $params;

  protected abstract function getIcon(): string;
  protected abstract function getTitle(): string;
  protected abstract function getPanelBody(): string;
  public abstract function collectData(): void;

  public function __construct(\Joomla\Registry\Registry $params)
  {
    $this->params = $params;
  }

  protected function getLabel(): string
  {
    return '';
  }

  protected function hasData(): bool
  {
    return !empty($this->data);
  }

  /**
   * @inheritDoc
   */
  public function getTab()
  {
    $this->collectData();

    $html = '';
    if ($this->hasData()) {
      $html = $this->getIcon();
      if ($label = $this->getLabel())
        $html .= ' <span class="tracy-label">' . $label . '</span>';
    }

    return $html;
  }

  /**
   * @inheritDoc
   */
  public function getPanel()
  {
    $html = '';

    if ($this->hasData() && ($panelBody = $this->getPanelBody())) {
      $html = '<h1>' . $this->getIcon() . ' ' . $this->getTitle() . '</h1>';
      $html .= '<div class="tracy-inner"><div class="tracy-inner-container">';
      $html .= $panelBody;
      $html .= '</div></div>';
    }

    return $html;
  }

  protected function renderCallStack(array $callStack = [])
  {
    $htmlCallStack = '';

    if ($callStack)
    {
      $htmlCallStack.= '<div>';
      $htmlCallStack.= '<table class="table table-striped dbg-query-table">';
      $htmlCallStack.= '<thead>';
      $htmlCallStack.= '<th>#</th>';
      $htmlCallStack.= '<th>Caller</th>';
      $htmlCallStack.= '<th>File And Line</th>';
      $htmlCallStack.= '</tr>';
      $htmlCallStack.= '</thead>';
      $htmlCallStack.= '<tbody>';

      $count = count($callStack);

      foreach ($callStack as $call)
      {
        if (isset($call['class'])) {
          if (in_array($call['class'], ['JLog', 'Joomla\\CMS\\Log\\LogEntry', 'Joomla\\CMS\\Log\\Log']) !== false) {
            $count--;
            continue;
          }
        }

        $htmlCallStack.= '<tr>';
        $htmlCallStack.= '<td>' . $count . '</td>';
        $htmlCallStack.= '<td>';

        if (isset($call['class'])) {
          $htmlCallStack.= htmlspecialchars($call['class'] . $call['type'] . $call['function']) . '()';
        } else {
          if (isset($call['args']))
            $htmlCallStack.= htmlspecialchars($call['function']) . ' ' . Helper::fileLink($call['args'][0]);
          else
            $htmlCallStack.= htmlspecialchars($call['function']) . '()';
        }

        $htmlCallStack.= '</td>';
        $htmlCallStack.= '<td>';

        if (!isset($call['file']) && !isset($call['line']))
          $htmlCallStack.= 'same file';
        else
          $htmlCallStack.= Helper::fileLink($call['file'], $call['line']);

        $htmlCallStack.= '</td>';
        $htmlCallStack.= '</tr>';

        $count--;
      }

      $htmlCallStack.= '</tbody>';
      $htmlCallStack.= '</table>';
      $htmlCallStack.= '</div>';
    }

    return $htmlCallStack;
  }

  protected function getDocument()
  {
    if (Version::MAJOR_VERSION == 3)
      return Factory::getDocument();

    return Factory::getApplication()->getDocument();
  }

  protected function getInput()
  {
    $app = Factory::getApplication();

    if (Version::MAJOR_VERSION == 3)
      return $app->input;

    return $app->getInput();
  }

  protected function getConfig()
  {
    if (Version::MAJOR_VERSION == 3)
      return Factory::getConfig();

    return Factory::getApplication()->getConfig();
  }

  protected function getLanguage()
  {
    if (Version::MAJOR_VERSION == 3)
      return Factory::getLanguage();

    return Factory::getApplication()->getLanguage();
  }

  protected function getDbo()
  {
    if (Version::MAJOR_VERSION == 3)
      return Factory::getDbo();

    return Factory::getContainer()->get('DatabaseDriver');
  }

  protected function getSession()
  {
    if (Version::MAJOR_VERSION == 3)
      return Factory::getSession();

    return Factory::getApplication()->getSession();
  }

  protected function getUser()
  {
    if (Version::MAJOR_VERSION == 3)
      return Factory::getUser();

    return Factory::getApplication()->getIdentity();
  }

  protected function isSessionActive(): bool
  {
    return $this->getSession()->isActive();
  }
}
