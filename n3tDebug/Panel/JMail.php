<?php
/**
 * @package n3t Debug
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2016-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

namespace n3tDebug\Panel;

defined( '_JEXEC' ) or die;

use n3tDebug\Helper;
use n3tDebug\Joomla\Mail;
use Joomla\Registry\Registry;

class JMail extends \n3tDebug\Panel
{

  public function __construct(Registry $params)
  {
    parent::__construct($params);
    Mail::getInstance();
  }

  protected function getIcon(): string
  {
    $color = Helper::COLOR_SUCCESS;
    foreach ($this->data as $mail)
      if ($mail->isError())
        $color = Helper::COLOR_ERROR;

    return '<svg viewBox="0 0 16 16" width="32" height="32" style="vertical-align: text-bottom">' .
      '<path fill="' . $color . '" d="M14.5 2h-13c-0.825 0-1.5 0.675-1.5 1.5v10c0 0.825 0.675 1.5 1.5 1.5h13c0.825 0 1.5-0.675 1.5-1.5v-10c0-0.825-0.675-1.5-1.5-1.5zM6.23 8.6l-4.23 3.295v-7.838l4.23 4.543zM2.756 4h10.488l-5.244 3.938-5.244-3.938zM6.395 8.777l1.605 1.723 1.605-1.723 3.29 4.223h-9.79l3.29-4.223zM9.77 8.6l4.23-4.543v7.838l-4.23-3.295z"></path>' .
      '</svg>';
  }

  protected function getTitle(): string
  {
    return sprintf('%d sent emails', count($this->data));
  }

  protected function getPanelBody(): string
  {
    $html = '<table><thead>';
    $html.= '<tr><th>#</th><th>Result</th><th>Details</th><th>To</th><th>Subject</th></tr>';
    $html.= '</thead><tbody>';

    foreach ($this->data as $index => $mail) {
      $html.= '<tr>';
      $html.= '<td>' . ($index + 1) . '.</td>';
      if ($mail->isError())
        $html.= '<td><strong>Error</strong></td>';
      else
        $html.= '<td>Success</td>';
      $recipients = $mail->getToAddresses();
      $id = uniqid('tracy-addons-JMail-');
      $html.= '<td><a href="#' . $id . '" class="tracy-toggle tracy-collapsed" rel="#tracy-debug-n3tdebug-row-mail-' . $id . '">details&hellip;</a></td>';
      $html.= '<td>' . $recipients[0][0] . '.</td>';
      $html.= '<td>' . $mail->Subject . '.</td>';
      $html.= '</tr>';
      $html.= '<tr id="tracy-debug-n3tdebug-row-mail-' . $index . '" class="tracy-collapsed">';
      $html.= '<td colspan="5">' . \Tracy\Dumper::toHtml($mail) . '</td>';
      $html.= '</tr>';
    }

    $html.= '</tbody></table>';

    return $html;
  }

  public function collectData(): void
  {
    $this->data = Mail::getLog();
  }
}
