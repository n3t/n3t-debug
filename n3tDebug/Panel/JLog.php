<?php
/**
 * @package n3t Debug
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2016-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

namespace n3tDebug\Panel;

defined( '_JEXEC' ) or die;

use Joomla\CMS\Log\Log;
use Joomla\CMS\Log\LogEntry;
use Joomla\Registry\Registry;
use n3tDebug\Helper;
use n3tDebug\Panel;

class JLog extends Panel
{
  private $priorities = array(
    Log::EMERGENCY => 'Emergency',
    Log::ALERT => 'Alert',
    Log::CRITICAL => 'Critical',
    Log::ERROR => 'Error',
    Log::WARNING => 'Warning',
    Log::NOTICE => 'Notice',
    Log::INFO => 'Info',
    Log::DEBUG => 'Debug',
  );

  private $entries = [];

  public function __construct(Registry $params)
  {
    parent::__construct($params);

    Log::addLogger([
      'logger' => 'Callback',
      'callback' => [$this, 'log'],
    ], Log::ALL, [], true);
  }

  public function collectData(): void
  {
    if (!$this->hasData())
      return;

    $this->entries = [];

    foreach ($this->data as $entry) {
      if (!isset($this->entries[$entry->priority]))
        $this->entries[$entry->priority] = [];
      $this->entries[$entry->priority][] = $entry;
    }
    ksort($this->entries);
  }

  public function log(LogEntry $entry): void
  {
    $this->data[] = $entry;
  }

  protected function getIcon(): string
  {
    switch (array_key_first($this->entries)) {
      case Log::EMERGENCY:
      case Log::ALERT:
      case Log::CRITICAL:
      case Log::ERROR:
        $color = Helper::COLOR_ERROR;
        break;
      case Log::WARNING:
      case Log::NOTICE:
        $color = Helper::COLOR_WARNING;
        break;
      case Log::INFO:
      case Log::DEBUG:
        $color = Helper::COLOR_INFO;
        break;
      default:
        $color = Helper::COLOR_SUCCESS;
        break;
    }

    return '<svg viewBox="0 0 16 16" width="32" height="32" style="vertical-align: text-bottom">' .
      '<path fill="' . $color . '" d="M14.341 3.579c-0.347-0.473-0.831-1.027-1.362-1.558s-1.085-1.015-1.558-1.362c-0.806-0.591-1.197-0.659-1.421-0.659h-7.75c-0.689 0-1.25 0.561-1.25 1.25v13.5c0 0.689 0.561 1.25 1.25 1.25h11.5c0.689 0 1.25-0.561 1.25-1.25v-9.75c0-0.224-0.068-0.615-0.659-1.421zM12.271 2.729c0.48 0.48 0.856 0.912 1.134 1.271h-2.406v-2.405c0.359 0.278 0.792 0.654 1.271 1.134zM14 14.75c0 0.136-0.114 0.25-0.25 0.25h-11.5c-0.135 0-0.25-0.114-0.25-0.25v-13.5c0-0.135 0.115-0.25 0.25-0.25 0 0 7.749-0 7.75 0v3.5c0 0.276 0.224 0.5 0.5 0.5h3.5v9.75z"></path>' .
      '<path fill="' . $color . '" d="M11.5 13h-7c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h7c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5z"></path>' .
      '<path fill="' . $color . '" d="M11.5 11h-7c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h7c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5z"></path>' .
      '<path fill="' . $color . '" d="M11.5 9h-7c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h7c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5z"></path>' .
      '</svg>';
  }

  protected function getTitle(): string
  {
    return 'Joomla Log';
  }

  protected function getPanelBody(): string
  {
    $html = '';

    foreach ($this->entries as $priority => $entries) {
      $id = uniqid('tracy-addons-JLog-');
      $html.= '<h2><a href="#' . $id . '" class="tracy-toggle tracy-collapsed">' . $this->priorities[$priority] . '</a></h2>';
      $html.= '<table class="tracy-sortable tracy-collapsed" id="' . $id . '">';
      $html.= '<tbody>';
      $html.= '<tr><th>Category</th><th>Message</th></tr>';
      foreach ($entries as $entry) {
        $html.= '<tr>';
        $html.= '<td>' . $entry->category . '</td>';
        $html.= '<td>' . $entry->message . '<br/>';
        $idCallStack = uniqid('tracy-debug-n3tdebug-row-callstack-');
        $html .= '<a href="#' . $idCallStack . '" class="tracy-toggle tracy-collapsed">Call stack</a>';
        $html .= '<div id="' . $idCallStack . '" class="tracy-collapsed">' . $this->renderCallStack($entry->callStack) . '</div>';
        $html.= '</tr>';
        $html.= '<tr>';

        $html.= '</tr>';
      }
      $html.= '</tbody></table>';
    }

    return $html;
  }
}
