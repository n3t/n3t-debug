<?php
/**
 * @package n3t Debug
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2016-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

namespace n3tDebug\Panel;

defined( '_JEXEC' ) or die;

use n3tDebug\Helper;
use n3tDebug\Panel;
use n3tDebug\Joomla\Profiler;

class JProfile extends Panel {

  public function collectData(): void
  {
    $this->data = Profiler::getInstances();
    $this->data = array_filter($this->data, function ($value) {
      return !empty($value->getMarks());
    });
    ksort($this->data, SORT_STRING );
  }

  protected function getIcon(): string
  {
    $color = Helper::COLOR_INFO;

    return '<svg viewBox="0 0 16 16" width="32" height="32" style="vertical-align: text-bottom">' .
      '<path fill="' . $color . '" d="M8 3.019v-1.019h2v-1c0-0.552-0.448-1-1-1h-3c-0.552 0-1 0.448-1 1v1h2v1.019c-3.356 0.255-6 3.059-6 6.481 0 3.59 2.91 6.5 6.5 6.5s6.5-2.91 6.5-6.5c0-3.422-2.644-6.226-6-6.481zM11.036 13.036c-0.944 0.944-2.2 1.464-3.536 1.464s-2.591-0.52-3.536-1.464c-0.944-0.944-1.464-2.2-1.464-3.536s0.52-2.591 1.464-3.536c0.907-0.907 2.101-1.422 3.377-1.462l-0.339 4.907c-0.029 0.411 0.195 0.591 0.497 0.591s0.527-0.18 0.497-0.591l-0.339-4.907c1.276 0.040 2.47 0.555 3.377 1.462 0.944 0.944 1.464 2.2 1.464 3.536s-0.52 2.591-1.464 3.536z"></path>' .
      '</svg>';
  }

  protected function getTitle(): string
  {
    return 'Joomla profile information';
  }

  protected function getPanelBody(): string
  {
    $html = '';
    $index = 0;
    foreach ($this->data as $name => $instance) {
      $marks = $instance->getMarks();
      $totalTime = 0;
      $totalMemory = 0;
      $maxMemory = 0;

      foreach ($marks as $mark) {
        $totalTime += (float)$mark->time;
        $totalMemory += (float)$mark->memory;
        $maxMemory = ((float)$mark->totalMemory > $maxMemory) ? (float)$mark->totalMemory : $maxMemory;
      }

      $avgTime = $totalTime / max(count($marks), 1);
      $avgMemory = $totalMemory / max(count($marks), 1);

      $id = uniqid('tracy-addons-JProfile-');
      $html.= '<h2><a href="#' . $id . '" class="tracy-toggle tracy-collapsed">' . $name . '</a></h2>';
      $html.= '<p>Total time: <strong>' . sprintf('%.2f&nbsp;ms', $totalTime) . '</strong>, Max. memory: <strong>' . sprintf('%0.3f&nbsp;MB', $maxMemory) . '</strong></p>';
      $html.= '<table id="' . $id . '" class="tracy-collapsed">';
      $html.= '<thead><tr><th>Time</th><th>Label</th><th>Time diff</th><th>Memory diff</th><th>Memory total</th></tr></thead>';
      $html.= '<tbody>';

      foreach ($marks as $mark) {
        $html.= '<tr>';
        $html.= '<td><strong>' . sprintf('%.2f&nbsp;ms', $mark->totalTime) . '</strong></td>';
        $html.= '<td>' . $mark->prefix . ': ' . $mark->label . '</td>';
        if ($mark->time > $avgTime * 1.5)
          $html.= '<td><strong>' . sprintf('%.2f&nbsp;ms', $mark->time) . '</strong></td>';
        else
          $html.= '<td>' . sprintf('%.2f&nbsp;ms', $mark->time) . '</td>';
        if ($mark->memory > $avgMemory * 1.5)
          $html.= '<td><strong>' . sprintf('%0.3f&nbsp;MB', $mark->memory) . '</strong></td>';
        else
          $html.= '<td>' . sprintf('%0.3f&nbsp;MB', $mark->memory) . '</td>';
        $html.= '<td>' . sprintf('%0.3f&nbsp;MB', $mark->totalMemory) . '</td>';
        $html.= '</tr>';
      }

      $html.= '</tbody></table>';
    }

    return $html;
  }

}
