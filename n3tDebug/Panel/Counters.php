<?php
/**
 * @package n3t Debug
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2016-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

namespace n3tDebug\Panel;

defined( '_JEXEC' ) or die;

use n3tDebug\Helper;
use n3tDebug\Panel;

class Counters extends Panel {

  public function collectData(): void
  {
    $data = \n3tDebug::getCounters();
    if ($data) {
      $this->data = $data;
    }
  }

  protected function getIcon(): string
  {
    return '<svg viewBox="0 0 1024 1024" width="32" height="32" style="vertical-align: text-bottom">' .
      '<path fill="' . Helper::COLOR_ERROR . '"  d="M170.6 170.6h682.6a85.3 85.3 0 0 1 85.3 85.3v512a85.3 85.3 0 0 1-85.3 85.3H170.6a85.3 85.3 0 0 1-85.3-85.3V256a85.3 85.3 0 0 1 85.3-85.3m0 85.3v512h298.6V256H170.6m682.6 512V256h-52.9c10.2 23.0 8.1 45.6 8.1 48.2-2.9 28.5-23.0 58.4-30.2 69.1l-99.4 108.8 141.6-0.8 0.4 52.0-221.8-1.2-1.7-42.6s130.1-137.8 136.5-150.1c5.9-11.9 30.2-83.2-29.8-83.2-52.4 2.1-46.5 55.4-46.5 55.4l-65.7 0.4s0.4-28.1 16.2-55.8H554.6v512h110.0l-0.4-36.6 41.3-0.4s38.8-6.8 39.2-44.8c1.7-42.6-34.5-42.6-40.9-42.6-5.5 0-45.6 2.1-45.6 37.1h-64.8s1.7-87.8 110.5-87.8c110.9 0 104.9 86.1 104.9 86.1s1.7 53.3-47.3 73.3l22.1 15.7H853.3M380.5 682.6h-64v-247.4l-76.8 23.8V406.6l133.9-47.7h6.8V682.6z" />' .
      '</svg> ';
  }

  protected function getTitle(): string
  {
    return 'Counters';
  }

  protected function getPanelBody(): string
  {
    $html = '<table class="tracy-sortable"><tbody>';

    foreach($this->data as $name => $value) {
      $html.= '<tr>';
      $html.= '<td>'.$name.'</td>';
      $html.= '<td>'.\Tracy\Dumper::toHtml($value, array(\Tracy\Dumper::COLLAPSE => true)).'</td>';
      $html.= '<tr>';
    }

    $html.= '</tbody></table>';

    return $html;
  }

}
