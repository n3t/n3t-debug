<?php
/**
 * @package n3t Debug
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2016-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

namespace n3tDebug\Panel;

use Joomla\CMS\Version;
use n3tDebug\Helper;

defined( '_JEXEC' ) or die;

class JStyleSheets extends \n3tDebug\Panel
{

  protected function getIcon(): string
  {
    return '<svg viewBox="0 0 40 32" width="40" height="32" style="vertical-align: text-bottom">' .
      '<path fill="' . Helper::COLOR_INFO . '" d="M8 11v-4c0-1.657 1.343-3 3-3h1v-4h-1c-3.866 0-7 3.134-7 7v4c0 1.657-1.343 3-3 3h-1v4h1c1.657 0 3 1.343 3 3v4c0 3.866 3.134 7 7 7h1v-4h-1c-1.657 0-3-1.343-3-3v-4c0-1.959-0.805-3.729-2.102-5 1.297-1.271 2.102-3.041 2.102-5z"></path>' .
      '<path fill="' . Helper::COLOR_INFO . '" d="M32 11v-4c0-1.657-1.343-3-3-3h-1v-4h1c3.866 0 7 3.134 7 7v4c0 1.657 1.343 3 3 3h1v4h-1c-1.657 0-3 1.343-3 3v4c0 3.866-3.134 7-7 7h-1v-4h1c1.657 0 3-1.343 3-3v-4c0-1.959 0.805-3.729 2.102-5-1.297-1.271-2.102-3.041-2.102-5z"></path>' .
      '<path fill="' . Helper::COLOR_ERROR . '" d="M24 10.028c0 2.209-1.791 4-4 4s-4-1.791-4-4c0-2.209 1.791-4 4-4s4 1.791 4 4z"></path>' .
      '<path fill="' . Helper::COLOR_ERROR . '" d="M20 15.972c-2.209 0-4 1.791-4 4 0 2.148 1 3.851 4.067 3.995-0.912 1.285-2.286 1.793-4.067 1.959v2.375c0 0 8 0.698 8-8.33-0.008-2.209-1.791-4-4-4z"></path>' .
      '</svg>';
  }

  protected function getTitle(): string
  {
    return 'StyleSheets (' . count($this->data) . ')';
  }

  protected function getPanelBody(): string
  {
    $html = '<table class="tracy-sortable"><thead>';
    $html.= '<tr><th>StyleSheet</th><th>Options</th></tr>';
    $html.= '</thead><tbody>';
    foreach($this->data as $name => $value) {
      $html.= '<tr>';
      $html.= '<td>' . $name . '</td>';
      $html.= '<td>' . \Tracy\Dumper::toHtml($value, array(\Tracy\Dumper::COLLAPSE => true)) . '</td>';
      $html.= '</tr>';
    }

    $html.= '</tbody></table>';

    return $html;
  }

  public function collectData(): void
  {
    $doc = $this->getDocument();
    if ($doc) {
      if (Version::MAJOR_VERSION == 3)
        $this->data = $doc->_styleSheets;
      else
        $this->data = $doc->getWebAssetManager()->getAssets('style');
    }
  }
}
