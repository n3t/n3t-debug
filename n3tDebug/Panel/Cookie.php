<?php
/**
 * @package n3t Debug
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2016-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

namespace n3tDebug\Panel;

defined( '_JEXEC' ) or die;

use n3tDebug\Helper;
use n3tDebug\Panel;

class Cookie extends Panel
{

  public function collectData(): void
  {
    if (isset($_COOKIE) && is_array($_COOKIE) && count($_COOKIE)) {
      $this->data = $_COOKIE;
      ksort($this->data);
    }
  }

  protected function getIcon(): string
  {
    $html = '';

    if (isset($_COOKIE) && is_array($_COOKIE) && count($_COOKIE)) {
      $html = '<svg viewBox="0 0 16 16" width="32" height="32" style="vertical-align: text-bottom">' .
        '<path fill="' . Helper::COLOR_INFO . '" d="M8 16c4.418 0 8-3.582 8-8s-3.582-8-8-8-8 3.582-8 8 3.582 8 8 8zM8 1.5c3.59 0 6.5 2.91 6.5 6.5s-2.91 6.5-6.5 6.5-6.5-2.91-6.5-6.5 2.91-6.5 6.5-6.5zM4 5c0-0.552 0.448-1 1-1s1 0.448 1 1c0 0.552-0.448 1-1 1s-1-0.448-1-1zM10 5c0-0.552 0.448-1 1-1s1 0.448 1 1c0 0.552-0.448 1-1 1s-1-0.448-1-1zM11.002 9.801l1.286 0.772c-0.874 1.454-2.467 2.427-4.288 2.427s-3.413-0.973-4.288-2.427l1.286-0.772c0.612 1.018 1.727 1.699 3.002 1.699s2.389-0.681 3.002-1.699z"></path>' .
        '</svg>';
    }

    return $html;
  }

  protected function getTitle(): string
  {
    return '$_COOKIE';
  }

  protected function getPanelBody(): string
  {
    $html = '<table class="tracy-sortable"><tbody>';

    foreach($this->data as $name => $value) {
      $html.= '<tr>';
      $html.= '<td>'.$name.'</td>';
      $html.= '<td>'.\Tracy\Dumper::toHtml($value, array(\Tracy\Dumper::COLLAPSE => true)).'</td>';
      $html.= '<tr>';
    }

    $html.= '</tbody></table>';

    return $html;
  }

}
