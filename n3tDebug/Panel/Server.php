<?php
/**
 * @package n3t Debug
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2016-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

namespace n3tDebug\Panel;

defined( '_JEXEC' ) or die;

use n3tDebug\Helper;
use n3tDebug\Panel;

class Server extends Panel {

  public function collectData(): void
  {
    if (isset($_SERVER) && is_array($_SERVER) && count($_SERVER)) {
      $this->data =$_SERVER;
      ksort($this->data);
    }
  }

  protected function getIcon(): string
  {
    return '<svg viewBox="0 0 16 16" width="32" height="32" style="vertical-align: text-bottom">' .
      '<path fill="' . Helper::COLOR_INFO . '" d="M13 0h-10l-3 4h16l-3-4z"></path>' .
      '<path fill="' . Helper::COLOR_INFO . '" d="M0 5v3h16v-3h-16zM15 7h-1v-1h1v1z"></path>' .
      '<path fill="' . Helper::COLOR_INFO . '" d="M0 9v3h16v-3h-16zM15 11h-1v-1h1v1z"></path>' .
      '<path fill="' . Helper::COLOR_INFO . '" d="M0 13v3h16v-3h-16zM15 15h-1v-1h1v1z"></path>' .
      '</svg> ';
  }

  protected function getTitle(): string
  {
    return '$_SERVER';
  }

  protected function getPanelBody(): string
  {
    $html = '<table class="tracy-sortable"><tbody>';

    foreach($this->data as $name => $value) {
      $html.= '<tr>';
      $html.= '<td>'.$name.'</td>';
      $html.= '<td>'.\Tracy\Dumper::toHtml($value, array(\Tracy\Dumper::COLLAPSE => true)).'</td>';
      $html.= '<tr>';
    }

    $html.= '</tbody></table>';

    return $html;
  }

}
