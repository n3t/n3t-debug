<?php
/**
 * @package n3t Debug
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2016-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

namespace n3tDebug\Panel;

defined( '_JEXEC' ) or die;

use n3tDebug\Helper;
use n3tDebug\Panel;

class JUser extends Panel {

  public function collectData(): void
  {
    if ($this->hasData())
      return;

    if (!$this->isSessionActive())
      return;

    $this->data = $this->getUser();
  }

  protected function getIcon(): string
  {
    $color = $this->data->guest ? Helper::COLOR_INFO : Helper::COLOR_SUCCESS;

    return '<svg viewBox="0 0 32 32" width="32" height="32" style="vertical-align: text-bottom">' .
      '<path fill="' . $color . '" d="M18 22.082v-1.649c2.203-1.241 4-4.337 4-7.432 0-4.971 0-9-6-9s-6 4.029-6 9c0 3.096 1.797 6.191 4 7.432v1.649c-6.784 0.555-12 3.888-12 7.918h28c0-4.030-5.216-7.364-12-7.918z"></path>' .
      '</svg>';
  }

   protected function getLabel(): string
   {
     if ($this->data->guest)
       return 'Guest';
     else
       return $this->data->username;
   }

   protected function getTitle(): string
   {
     if ($this->data->guest)
       return 'Guest';
     else
       return $this->data->username . ' <small>(' . $this->data->name . ')</small>';
   }

   protected function getPanelBody(): string
   {
     $html = '<table class="tracy-sortable">';
     $html.= '<thead><tr><th>Key</th><th>Value</th></tr></thead>';
     $html.= '<tbody>';
     foreach ($this->data as $name => $value) {
       $html.= '<tr>';
       $html.= '<td>' . $name . '</td>';
       $html.= '<td>' . \Tracy\Dumper::toHtml($value, array(\Tracy\Dumper::COLLAPSE => true)) . '</td>';
       $html.= '<tr>';
     }
     $html.= '</tbody></table>';

     return $html;
   }

}
