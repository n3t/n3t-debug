<?php
/**
 * @package n3t Debug
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2016-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

namespace n3tDebug\Panel;

defined( '_JEXEC' ) or die;

use Joomla\CMS\Version;
use n3tDebug\Helper;

class JScripts extends \n3tDebug\Panel
{

  protected function getIcon(): string
  {
    return '<svg viewBox="0 0 40 32" width="40" height="32" style="vertical-align: text-bottom">' .
      '<path fill="' . Helper::COLOR_INFO . '" d="M26 23l3 3 10-10-10-10-3 3 7 7z"></path>' .
      '<path fill="' . Helper::COLOR_INFO . '" d="M14 9l-3-3-10 10 10 10 3-3-7-7z"></path>' .
      '<path fill="' . Helper::COLOR_ERROR . '" d="M21.916 4.704l2.171 0.592-6 22.001-2.171-0.592 6-22.001z"></path>' .
      '</svg>';
  }

  protected function getTitle(): string
  {
    return 'Scripts (' . count($this->data) . ')';
  }

  protected function getPanelBody(): string
  {
    $html = '<table class="tracy-sortable"><thead>';
    $html.= '<tr><th>Script</th><th>Options</th></tr>';
    $html.= '</thead><tbody>';
    foreach($this->data as $name => $value) {
      $html.= '<tr>';
      $html.= '<td>' . $name . '</td>';
      $html.= '<td>' . \Tracy\Dumper::toHtml($value, array(\Tracy\Dumper::COLLAPSE => true)) . '</td>';
      $html.= '</tr>';
    }

    $html.= '</tbody></table>';

    return $html;
  }

  public function collectData(): void
  {
    $doc = $this->getDocument();
    if ($doc) {
      if (Version::MAJOR_VERSION == 3)
        $this->data = $doc->_scripts;
      else
        $this->data = $doc->getWebAssetManager()->getAssets('script');
    }
  }
}
