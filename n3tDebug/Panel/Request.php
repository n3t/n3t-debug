<?php
/**
 * @package n3t Debug
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2016-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

namespace n3tDebug\Panel;

defined( '_JEXEC' ) or die;

use n3tDebug\Helper;
use n3tDebug\Panel;

class Request extends Panel {

  public function collectData(): void
  {
    if (isset($_REQUEST) && is_array($_REQUEST) && count($_REQUEST)) {
      $this->data = $_REQUEST;
      ksort($this->data);
    }
  }

  protected function getIcon(): string
  {
    return '<svg viewBox="0 0 16 16" width="32" height="32" style="vertical-align: text-bottom">' .
      '<path fill="' . Helper::COLOR_INFO . '" d="M7.5 9l-1-1-3 3 3 3 1-1-2-2 2-2z"></path>' .
      '<path fill="' . Helper::COLOR_INFO . '" d="M8.5 13l1 1 3-3-3-3-1 1 2 2-2 2z"></path>' .
      '<path fill="' . Helper::COLOR_GRAY . '" d="M14.341 3.579c-0.347-0.473-0.831-1.027-1.362-1.558s-1.085-1.015-1.558-1.362c-0.806-0.591-1.197-0.659-1.421-0.659h-7.75c-0.689 0-1.25 0.561-1.25 1.25v13.5c0 0.689 0.561 1.25 1.25 1.25h11.5c0.689 0 1.25-0.561 1.25-1.25v-9.75c0-0.224-0.068-0.615-0.659-1.421v0zM12.271 2.729c0.48 0.48 0.856 0.912 1.134 1.271h-2.406v-2.405c0.359 0.278 0.792 0.654 1.271 1.134v0zM14 14.75c0 0.136-0.114 0.25-0.25 0.25h-11.5c-0.135 0-0.25-0.114-0.25-0.25v-13.5c0-0.135 0.115-0.25 0.25-0.25 0 0 7.749-0 7.75 0v3.5c0 0.276 0.224 0.5 0.5 0.5h3.5v9.75z"></path>' .
      '</svg> ';
  }

  protected function getTitle(): string
  {
    return '$_REQUEST';
  }

  protected function getPanelBody(): string
  {
    $html = '<table class="tracy-sortable"><tbody>';

    foreach($this->data as $name => $value) {
      $html.= '<tr>';
      $html.= '<td>'.$name.'</td>';
      $html.= '<td>'.\Tracy\Dumper::toHtml($value, array(\Tracy\Dumper::COLLAPSE => true)).'</td>';
      $html.= '<tr>';
    }

    $html.= '</tbody></table>';

    return $html;
  }

}
