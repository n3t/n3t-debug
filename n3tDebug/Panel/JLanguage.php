<?php
/**
 * @package n3t Debug
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2016-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

namespace n3tDebug\Panel;

defined( '_JEXEC' ) or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Version;
use Joomla\Registry\Registry;
use n3tDebug\Helper;
use n3tDebug\Joomla\Language;
use n3tDebug\Panel;

class JLanguage extends Panel {

  private $ignoreClassList = [
    'Joomla\\CMS\\Language\\Language',
    '\\Joomla\\CMS\\Language\\Language',
    'JText',
    '\\JText',
    'Joomla\\CMS\\Language\\Text',
    '\\Joomla\\CMS\\Language\\Text',
    'n3tDebug\\Joomla\\Language',
    '\\n3tDebug\\Joomla\\Language',
  ];

  public function __construct(Registry $params)
  {
    parent::__construct($params);

    $conf = $this->getConfig();
    $locale = $conf->get('language');
    $paths = Factory::$language->getPaths();

    Factory::$language = new Language($locale, !!$params->get('language_untranslated', 0));
    Factory::$language->reload($paths);
    Factory::getApplication()->loadLanguage(Factory::$language);
  }

  public function collectData(): void
  {
    $lang = $this->getLanguage();
    $this->data = [
      'errorFiles' => [],
      'orphans' => [],
      'used' => [],
      'paths' => [],
    ];

    if ($errorFiles = $lang->getErrorFiles()) {
      if (Version::MAJOR_VERSION == 3) {
        array_walk($errorFiles, function (&$error, $errorFile) {
          $errorLines = str_replace($errorFile . ' : error(s) in line(s) ', '', $error);
          $error = explode(',', $errorLines);
          array_walk($error, function (&$line, $index) {
            $line = (int)$line;
          });
        });
      }
      $this->data['errorFiles'] = $errorFiles;
    }

    if ($orphans = $lang->getOrphans()) {
      $this->data['orphans'] = $orphans;
    }

    if ($used = $lang->getUsed()) {
      $this->data['used'] = $used;
    }

    if ($paths = $lang->getPaths()) {
      $this->data['paths'] = $paths;
    }
  }

  protected function getIcon($title = false): string
  {
    $lang = $this->getLanguage();
    if ($lang->getErrorFiles())
      $color = Helper::COLOR_ERROR;
    elseif ($lang->getOrphans())
      $color = Helper::COLOR_WARNING;
    else
      $color = Helper::COLOR_INFO;

    return '<svg viewBox="0 0 16 16" width="32" height="32" style="vertical-align: text-bottom">' .
      '<path fill="' . $color . '" d="M8 0c-4.418 0-8 3.582-8 8s3.582 8 8 8 8-3.582 8-8-3.582-8-8-8zM8 15c-0.984 0-1.92-0.203-2.769-0.57l3.643-4.098c0.081-0.092 0.126-0.21 0.126-0.332v-1.5c0-0.276-0.224-0.5-0.5-0.5-1.765 0-3.628-1.835-3.646-1.854-0.094-0.094-0.221-0.146-0.354-0.146h-2c-0.276 0-0.5 0.224-0.5 0.5v3c0 0.189 0.107 0.363 0.276 0.447l1.724 0.862v2.936c-1.813-1.265-3-3.366-3-5.745 0-1.074 0.242-2.091 0.674-3h1.826c0.133 0 0.26-0.053 0.354-0.146l2-2c0.094-0.094 0.146-0.221 0.146-0.354v-1.21c0.634-0.189 1.305-0.29 2-0.29 1.1 0 2.141 0.254 3.067 0.706-0.065 0.055-0.128 0.112-0.188 0.172-0.567 0.567-0.879 1.32-0.879 2.121s0.312 1.555 0.879 2.121c0.569 0.569 1.332 0.879 2.119 0.879 0.049 0 0.099-0.001 0.149-0.004 0.216 0.809 0.605 2.917-0.131 5.818-0.007 0.027-0.011 0.055-0.013 0.082-1.271 1.298-3.042 2.104-5.002 2.104z"></path>' .
      '</svg>';
  }

  protected function getTitle(): string
  {
    return 'Language';
  }

  private function getLoadedIcon(bool $success): string
  {
    if ($success)
      return '<svg width="16" height="16" viewBox="0 0 16 16">' .
        '<path fill="' . Helper::COLOR_SUCCESS . '" d="M14 2.5l-8.5 8.5-3.5-3.5-1.5 1.5 5 5 10-10z"></path>' .
        '</svg>';
    else
      return '<svg width="16" height="16" viewBox="0 0 16 16">' .
        '<path fill="' . Helper::COLOR_ERROR . '" d="M13.957 3.457l-1.414-1.414-4.543 4.543-4.543-4.543-1.414 1.414 4.543 4.543-4.543 4.543 1.414 1.414 4.543-4.543 4.543 4.543 1.414-1.414-4.543-4.543z"></path>' .
        '</svg>';
  }

  protected function getPanelBody(): string
  {
    $html = '';

    if ($errorFiles = $this->data['errorFiles']) {
      $id = uniqid('tracy-addons-JLanguage-errors');
      $html.= '<h2><a href="#' . $id .'" class="tracy-toggle">Parsing errors</a></h2>';
      $html.= '<table id="' . $id . '" class="tracy-sortable"><thead>';
      $html.= '<tr><th>File</th></tr>';
      $html.= '<thead><tbody>';
      foreach ($errorFiles as $errorFile => $lines) {
        $html.= '<tr><td>' . Helper::fileLink($errorFile, $lines) . '</td></tr>';
      }
      $html.= '</tbody></table>';
    }

    if ($orphans = $this->data['orphans']) {
      ksort($orphans, SORT_STRING);

      $id = uniqid('tracy-addons-JLanguage-orphans');
      $html.= '<h2><a href="#' . $id . '" class="tracy-toggle">Untranslated strings</a></h2>';
      $html.= '<table id="' . $id . '" class="tracy-sortable"><thead>';
      $html.= '<tr><th>Key</th><th>Occurances</th></tr>';
      $html.= '<thead><tbody>';
      foreach ($orphans as $key => $occurances) {
        $html.= '<tr><td><strong>' . $key . '</strong></td><td>';

        foreach ($occurances as $occurance)
          $html .= $this->occuranceLink($occurance);

        $html.= '</td></tr>';
      }
      $html.= '</tbody></table>';

    }

    if ($used = $this->data['used']) {
      ksort($used, SORT_STRING);
      $lang = $this->getLanguage();

      $overrideFile = JPATH_BASE . '/language/overrides/' . $lang->getTag() . '.override.ini';
      $overrideLink = 'editor://append/?file=' . urlencode($overrideFile) . '&line=0';

      $id = uniqid('tracy-addons-JLanguage-translated');
      $html.= '<h2><a href="#' . $id . '" class="tracy-toggle tracy-collapsed">Translated strings</a></h2>';
      $html.= '<table id="' . $id . '" class="tracy-sortable tracy-collapsed"><thead>';
      $html.= '<tr><th>&nbsp;</th><th>Key</th><th>Translation</th><th>Occurances</th></tr>';
      $html.= '<thead><tbody>';
      foreach ($used as $key => $occurances) {
        $translation = Text::_($key);
        $html.= '<tr><td><a href="' . $overrideLink . '&search=' . urlencode($key . '=') . '&replace=' . urlencode($key . '="' . $translation . '"') . '">Override</a></td>';
        $html.= '<td>' . $key . '</td>';
        $html.= '<td>' . $translation . '</td><td>';
        foreach ($occurances as $occurance)
          $html .= $this->occuranceLink($occurance);

        $html.= '</td></tr>';
      }
      $html.= '</tbody></table>';
    }

    if ($loadedFiles = $this->data['paths']) {
      $id = uniqid('tracy-addons-JLanguage-loaded');
      $html.= '<h2><a href="#' . $id . '" class="tracy-toggle tracy-collapsed">Loaded files</a></h2>';
      $html.= '<table id="' . $id . '" class="tracy-sortable tracy-collapsed"><thead>';
      $html.= '<tr><th>Status</th><th>File</th></tr>';
      $html.= '<thead><tbody>';
      foreach ($loadedFiles as $files) {
        foreach ($files as $file => $status) {
          $html.= '<tr><td>' . Helper::fileLink($file, 0, $status ? 'open' : 'create') . '</td>';
          $html.= '<td>' . $this->getLoadedIcon($status) . '</td></tr>';
        }
      }
      $html.= '</tbody></table>';
    }

    return $html;
  }

  private function occuranceLink($occurance): string
  {
    $file = '';
    $line = 0;

    if (isset($occurance['file'])) {
      $file = $occurance['file'];
      $line = $occurance['line'] ?? 0;
    } elseif (isset($occurance['trace'])) {
      $backtrace = $occurance['trace'];

      $continue = true;
      while ($continue && next($backtrace))
      {
        $step = current($backtrace);
        $class = isset($step['class']) ? $step['class'] : null;

        if ($class && !in_array($class, $this->ignoreClassList)) {
          $step = prev($backtrace);
          $file = $step['file'] ?? '';
          $line = $step['line'] ?? 0;

          $continue = false;
        }
      }
    }

    if ($file)
      return Helper::fileLink($file, $line) . '<br />';

    return '';
  }
}
