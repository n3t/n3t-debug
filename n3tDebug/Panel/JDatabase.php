<?php
/**
 * @package n3t Debug
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2016-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

namespace n3tDebug\Panel;

defined( '_JEXEC' ) or die;

use Joomla\CMS\Version;
use Joomla\Database\Monitor\DebugMonitor;
use n3tDebug\Helper;
use n3tDebug\Panel;
use Joomla\Registry\Registry;

class JDatabase extends Panel {

  private $db = null;
  private $hasExplain = false;
  private $hasExtendedExplain = false;
  private $hasProfiling = false;
  private $queryMonitor = null;

  private $showExplain = false;
  private $showProfile = false;
  private $showCallStack = false;
  private $maxQueries = 200;

  public function __construct(Registry $params)
  {
    parent::__construct($params);

    $this->showExplain = !!$params->get('database_explain', 0);
    $this->showProfile = !!$params->get('database_profile', 0);
    $this->showCallStack = !!$params->get('database_callstack', 0);
    $this->maxQueries = (int)$params->get('database_max_queries', 200);

    $this->db = $this->getDbo();

    $this->hasProfiling = $this->showProfile && (strpos($this->db->name, 'mysql') !== false) && version_compare($this->db->getVersion(), '5.0.37', '>=');
    $this->hasExplain = $this->showExplain && in_array($this->db->name, array('mysqli', 'mysql', 'pdomysql', 'postgresql'));
    $this->hasExtendedExplain = $this->hasExplain && (strpos($this->db->name, 'mysql') !== false) && version_compare($this->db->getVersion(), '5.6', '>=');

    if ($this->hasProfiling) {
      try {
        $this->db->setQuery("SHOW VARIABLES LIKE 'have_profiling'");
        $this->hasProfiling = !!$this->db->loadResult();
      } catch (\Exception $e) {
        // Ignore exception
      }
    }

    if ($this->hasProfiling) {
      $this->db->setQuery('SET profiling_history_size = 100, profiling = 1;');
      $this->db->execute();
    }

    if (Version::MAJOR_VERSION == 3) {
      $this->db->setDebug(true);
      $this->db->addDisconnectHandler(array($this, 'mysqlDisconnectHandler'));
    } else {
      $this->queryMonitor = $this->db->getMonitor();
      if (!$this->queryMonitor) {
        $this->queryMonitor = new DebugMonitor();
        $this->db->setMonitor($this->queryMonitor);
      }
    }

    \JLoader::register('SqlFormatter', __DIR__ . '/../../helpers/SqlFormatter.php');
  }

  protected function getIcon(): string
  {
    $color = Helper::COLOR_SUCCESS;

    return '<svg viewBox="0 0 16 16" width="32" height="32" style="vertical-align: text-bottom">' .
      '<path fill="' . $color . '" d="M8 0c-4.418 0-8 1.119-8 2.5v2c0 1.381 3.582 2.5 8 2.5s8-1.119 8-2.5v-2c0-1.381-3.582-2.5-8-2.5z"></path>' .
      '<path fill="' . $color . '" d="M8 8.5c-4.418 0-8-1.119-8-2.5v3c0 1.381 3.582 2.5 8 2.5s8-1.119 8-2.5v-3c0 1.381-3.582 2.5-8 2.5z"></path>' .
      '<path fill="' . $color . '" d="M8 13c-4.418 0-8-1.119-8-2.5v3c0 1.381 3.582 2.5 8 2.5s8-1.119 8-2.5v-3c0 1.381-3.582 2.5-8 2.5z"></path>' .
      '</svg>';
  }

  protected function getLabel(): string
  {
    return $this->data['totalQueries'] .' / ' . sprintf('%0.3f ms', $this->data['totalQueryTime'] * 1000);
  }

  protected function getTitle(): string
  {
    return 'Total queries: ' . $this->data['totalQueries'] . sprintf(', query time: %0.3f ms', $this->data['totalQueryTime'] * 1000);
  }

  protected function getPanelBody(): string
  {
    $html = '';
    $html.= '<style>';
    $html.= '#tracy-debug .tracy-n3tdebug-jdatabase td.tracy-n3tdebug-detail { background: white !important }';
    $html.= '#tracy-debug .tracy-n3tdebug-jdatabase table { margin: 8px 0; max-height: 150px; overflow:auto }';
    $html.= '</style>';

    if ($this->data['queries']) {
      $html.= '<table class="tracy-n3tdebug-jdatabase"><thead>';
      $html.= '<tr><th>#</th><th>SQL</th><th>Time&nbsp;[ms]</th><th>Duplicates</th>';
      if ($this->hasExplain)
        $html.= '<th>&nbsp;</th>';
      if ($this->hasProfiling)
        $html.= '<th>&nbsp;</th>';
      if ($this->showCallStack)
        $html.= '<th>&nbsp;</th>';
      $html.= '</tr>';
      $html.= '</thead><tbody>';

      foreach ($this->data['queries'] as $query) {
        $html.= '<tr>';
        $html.= '<td>' . $query['counter'] . '.</td>';
        $idSQL = uniqid('tracy-debug-n3tdebug-row-sql-');
        $html.= '<td>' . $this->truncateSQL($query['sql']) .  '<a href="#' . $idSQL . '" class="tracy-toggle tracy-collapsed">&hellip;</a></td>';

        if ($query['time'])
          $html.= '<td>' . sprintf('%0.3f ms', $query['time'] * 1000) . '</td>';
        else
          $html.= '<td>&nbsp;</td>';

        if (($duplicates = $query['duplicates']) && (count($duplicates) > 1)) {
          $duplicates = array_filter($duplicates, function ($item) use ($query) {
            return $item != $query['id'];
          });

          array_walk($duplicates, function (&$item, $index) {
            $item = '#' . ++$item;
          });

          $html .= '<td><strong>' . implode(', ', $duplicates) . '</strong></td>';
        } else
          $html.= '<td>&nbsp;</td>';

        if ($query['explain']) {
          $idExplain = uniqid('tracy-debug-n3tdebug-row-explain-');
          $html .= '<td><a href="#' . $idExplain . '" class="tracy-toggle tracy-collapsed">explain</a></td>';
        } elseif ($this->hasExplain)
          $html.= '<td>&nbsp;</td>';

        if ($query['profile']) {
          $idProfile = uniqid('tracy-debug-n3tdebug-row-profile-');
          $html .= '<td><a href="#' . $idProfile . '" class="tracy-toggle tracy-collapsed">profile</a></td>';
        } elseif ($this->hasProfiling)
          $html .= '<td>&nbsp;</td>';

        if ($query['callstack']) {
          $idCallStack = uniqid('tracy-debug-n3tdebug-row-callstack-');
          $html .= '<td><a href="#' . $idCallStack . '" class="tracy-toggle tracy-collapsed">call stack</a></td>';
        } elseif ($this->showCallStack)
          $html.= '<td>&nbsp;</td>';

        $html .= '</tr>';

        $html .= '<tr><td colspan="7" class="tracy-n3tdebug-detail">';

        $html .= '<div id="' . $idSQL . '" class="tracy-collapsed">' . $this->highlightSQL($query['sql']) . '</div>';
        if ($query['explain'])
          $html .= '<div id="' . $idExplain . '" class="tracy-collapsed">' . $this->renderExplain($query['explain']) . '</div>';
        if ($query['profile'])
          $html .= '<div id="' . $idProfile . '" class="tracy-collapsed">' . $this->renderProfile($query['profile']) . '</div>';
        if ($query['callstack'])
          $html .= '<div id="' . $idCallStack . '" class="tracy-collapsed">' . $this->renderCallStack($query['callstack']) . '</div>';

        $html .= '</td></tr>';
      }
      $html.= '</tbody></table>';
    }

    return $html;
  }

  public function mysqlDisconnectHandler(&$db)
  {
    $this->collectData();
  }

  public function collectData(): void
  {
    if ($this->hasData())
      return;
    $callStacks = [];
    if (Version::MAJOR_VERSION == 3) {
      $this->db->setDebug(false);
      $timings = $this->db->getTimings();
      if ($this->showCallStack)
        $callStacks = $this->db->getCallStacks();
      $log = $this->db->getLog();
    } else {
      $timings = $this->queryMonitor->getTimings();
      if ($this->showCallStack)
        $callStacks = $this->queryMonitor->getCallStacks();
      $log = $this->queryMonitor->getLogs();
    }

    $this->data['totalQueries'] = $this->db->getCount();

    $this->data['totalQueryTime'] = 0.0;
    if ($timings) {
      $lastStart = null;

      foreach ($timings as $k => $v) {
        if (!($k % 2))
          $lastStart = $v;
        else
          $this->data['totalQueryTime'] += $v - $lastStart;
      }
    }

    $this->data['queries'] = [];
    $duplicates = [];
    $profiling = [];

    if ($this->hasProfiling) {
      $this->db->setQuery('SHOW PROFILES');
      $sqlShowProfiles = $this->db->loadAssocList();

      if ($sqlShowProfiles) {
        foreach ($sqlShowProfiles as $qn) {
          $this->db->setQuery('SHOW PROFILE FOR QUERY ' . (int)($qn['Query_ID']));
          $profiling[(int)($qn['Query_ID'] - 1)] = $this->db->loadAssocList();
        }
      }
    }

    foreach ($log as $id => $sql) {
      if (count($this->data['queries']) >= $this->maxQueries)
        break;

      $query = [];
      $query['id'] = $id;
      $query['counter'] = $id + 1;
      $query['sql'] = $sql;
      $query['md5'] = md5($sql);

      if ($timings && isset($timings[$id * 2 + 1]))
        $query['time'] = ($timings[$id * 2 + 1] - $timings[$id * 2]);
      else
        $query['time'] = 0;
      $query['explain'] = null;
      $query['profile'] = null;
      if ($this->showCallStack)
        $query['callstack'] = $callStacks[$id];
      else
        $query['callstack'] = [];
      $query['duplicates'] = [];

      if (!isset($duplicates[$query['md5']]))
        $duplicates[$query['md5']] = [];
      $duplicates[$query['md5']][] = $id;

      if ($this->hasExplain) {
        if ((stripos($sql, 'select') === 0) || ($this->hasExtendedExplain && ((stripos($sql, 'delete') === 0) || (stripos($sql, 'update') === 0) || (stripos($sql, 'insert') === 0) || (stripos($sql, 'replace') === 0)))) {
          try {
            $this->db->setQuery('EXPLAIN ' . ($this->hasExtendedExplain ? 'EXTENDED ' : '') . $sql);
            $query['explain'] = $this->db->loadAssocList();
          } catch (\Exception $e) {
            // Ignore exception
          }
        }
      }

      if ($this->hasProfiling && isset($profiling[$id]))
        $query['profile'] = $profiling[$id];

      $this->data['queries'][] = $query;
    }

    foreach ($this->data['queries'] as &$query)
      $query['duplicates'] = $duplicates[$query['md5']];
  }

  private function highlightSQL($sql)
  {
    return \SqlFormatter::format($sql);
  }

  private function truncateSQL($sql)
  {
    $sql = \SqlFormatter::compress($sql);

    if (strlen($sql) > 30) {
      $len = strpos($sql, ' ', 20);
      if (!$len)
        $len = 30;
      if ($len > 30)
        $len = 30;
      $sql = substr($sql, 0, $len);
    }

    return $sql;
  }

  protected function renderExplain(array $explain = [])
  {
    $html = '<table><thead>';
    $html.= '<th>#</th>';
    $html.= '<th>Select type</th>';
    $html.= '<th>Table</th>';
    $html.= '<th>Type</th>';
    $html.= '<th>Possible keys</th>';
    $html.= '<th>Key</th>';
    $html.= '<th>Key length</th>';
    $html.= '<th>References</th>';
    $html.= '<th>Rows</th>';
    $html.= '<th>Extra</th>';
    $html.= '</tr>';
    $html.= '</thead><tbody>';

    foreach ($explain as $item)
    {
      $html.= '<tr>';
      $html.= '<td>' . $item['id'] . '</td>';
      $html.= '<td>' . $item['select_type'] . '</td>';
      $html.= '<td>' . $item['table'] . '</td>';
      $html.= '<td>' . $item['type'] . '</td>';
      $html.= '<td>' . $item['possible_keys'] . '</td>';
      $html.= '<td>' . $item['key'] . '</td>';
      $html.= '<td>' . $item['key_len'] . '</td>';
      $html.= '<td>' . $item['ref'] . '</td>';
      $html.= '<td>' . $item['rows'] . '</td>';
      $html.= '<td>' . $item['Extra'] . '</td>';
      $html.= '</tr>';
    }

    $html.= '</tbody></table>';

    return $html;
  }

  protected function renderProfile(array $profile = [])
  {
    $html = '';

    if ($profile) {
      $html.= '<table>';
      $html.= '<thead>';
      $html.= '<th>Status</th>';
      $html.= '<th>Duration</th>';
      $html.= '</tr>';
      $html.= '</thead>';
      $html.= '<tbody>';

      foreach ($profile as $item)
      {
        $html.= '<tr>';
        $html.= '<td>' . $item['Status'] . '</td>';
        $html.= '<td>' . sprintf('%.3f&nbsp;ms', $item['Duration'] * 1000) . '</td>';
        $html.= '</tr>';
      }

      $html.= '</tbody></table>';
    }

    return $html;
  }
}
