Release notes
=============

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

[Unreleased]
------------

- no unreleased changes

[5.0.x]
-------

### [5.0.3] - 2024-08-27

#### Changed
- 404 errors not handled by Tracy by default now.
#### Added
- Option to handle 404 errors.

### [5.0.2] - 2024-02-22

#### Changed
- n3tDebug::barDump now shows location, where it was called
#### Added
- Max. length, Max. depth and Keys to hide options to modify BlueScreen and dumps look. 

### [5.0.1] - 2024-01-09

#### Fixed
- Joomla 3.x backward compatibility

### [5.0.0] - 2023-10-24

#### Changed
- Improved way of measuring Joomla profiling information (Joomla inititalization time now included)
- Tracy 2.9.8
#### Added
- Joomla 5 compatibility
- label parameter for \n3tDebug::callstack function

[4.1.x]
-------

### [4.1.8] - 2023-08-29

#### Fixed
- fixed remote IP detection issue on MS Azure servers behind load balancer
- fixed unwritable tmp path on poorly configured servers


### [4.1.7] - 2023-03-30

#### Fixed
- disabled gzip compression in Joomla 3 (debug bar didn't render on some servers)
#### Changed
- Tracy 2.9.7

### [4.1.6] - 2023-02-26

#### Fixed
- disabled gzip compression and OB implicit flush in debug mode (debug bar didn't render on some servers)
#### Changed
- Language panel shows icons for load status of files now
- Profile panel now shows standard Joomla System events
#### Added
- new Counters panel

### [4.1.5] - 2023-02-07

#### Fixed
- Fatal error when plugin unpublished intorduced in 4.1.4 version
#### Changed
- Tracy 2.9.6

### [4.1.4] - 2023-02-02

#### Fixed
- Fatal error in Joomla 3 intorduced in 4.1.3 version
#### Added
- fields showing version of plugin and Tracy in administration

### [4.1.3] - 2022-12-12

#### Changed
- Tracy 2.9.5
- settings visualisation to follow Joomla 4 style
#### Added
- new Ignored levels option to enbale to globally switch of selected Error levels from displaying in panel.

### [4.1.2] - 2022-06-14

#### Changed
- small code changes to make JED happy 

### [4.1.1] - 2022-06-03

#### Added
- new n3tDebug::registerPanel helper function
#### Changed
- Tracy 2.9.3
- small code improvements to make JED happy

### [4.1.0] - 2022-01-26

- added option to register panel for other extensions
- Tracy 2.9.0

[4.0.x]
-------

### [4.0.7] - 2021-12-13
- improved J4 compatibility
- new E_NONE option in ignore error levels parameter

### [4.0.6] - 2021-12-10
- updated Tracy to 2.8.9
- additional Debugger configuration options in plugin config

### [4.0.5] - 2021-08-07
- installer script moved to package

### [4.0.4] - 2021-06-25
- rethrow error only when enabled

### [4.0.3] - 2021-06-25
- correct error page for Joomla 4  

### [4.0.2] - 2021-06-24
- IP list field buttons styling
- better Call Stack for Log Entries
- updated to Tracy 2.8.5

### [4.0.1] - 2021-05-22
- updated to Tracy 2.8.4
- register n3tDebug class even if debugging is not enabled, so users without debugging right will not get fatal error
- Advanced Database panel options
- Display current URL in Info panel

### [4.0.0] - 2021-04-10

- Joomla! 4 compatibility
- completely refactored code
- new svg icons, use basic Joomla colors
- new Joomla Info panel - basic info about Joomla
- new Joomla Language panel - language debugging
- new Joomla Profile panel - profiling info
- new Joomla Log Panel - display log entries
- new Joomla Mail Panel - info about sent emails
- new Joomla Scripts Panel - display loaded JavaScripts
- new Joomla StyleSheets Panel - display loaded StyleSheets
- redirects and AJAX calls (still not all) are now displayed

[3.0.x]
-------

### [3.0.2] - 2021-03-12

- Initial public release
- updated Tracy to 2.8.3
