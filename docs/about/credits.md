Credits
=======

Thanks to these projects and contributors:

- [Nette Tracy][TRACY]
- [Joomla! CMS][JOOMLA]
- [Czech Joomla community][JOOMLAPORTAL] for testing, support and comments on development

[TRACY]: https://tracy.nette.org
[JOOMLA]: https://www.joomla.org
[JOOMLAPORTAL]: https://www.joomlaportal.cz