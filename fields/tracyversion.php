<?php
/**
 * @package n3t Debug
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2016-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Form\FormField;
use Joomla\Registry\Registry;

class JFormFieldTracyVersion extends FormField
{
	protected $type = 'TracyVersion';

	protected function getInput()
	{
    $db = Factory::getDbo();
    $query = $db->getQuery(true)
      ->select($db->quoteName(['manifest_cache']))
      ->from($db->quoteName('#__extensions'))
      ->where($db->quoteName('name') . ' = ' . $db->quote('Tracy'));
    $db->setQuery($query);
    $extension = $db->loadObject();

    $manifest = new Registry($extension->manifest_cache);

    $input = '<span class="badge bg-primary">' . $manifest->get('version', '') . '</span>';
    return $input;
	}
}
