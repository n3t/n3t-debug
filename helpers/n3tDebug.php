<?php
/**
 * @package n3t Debug
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2016-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die;

use n3tDebug\Joomla\Profiler;
use Tracy\ILogger;

class n3tDebug {

  private static $plugin = null;
  private static $counters = [];

  public static function setPlugin(\plgSystemN3tDebug $plugin)
  {
    self::$plugin = $plugin;
  }

  public static function enabled()
  {
    return class_exists('\\Tracy\\Debugger');
  }

  public static function dump($var, bool $return = false)
  {
    if (!self::enabled()) return null;

    return \Tracy\Debugger::dump($var, $return);
  }

  public static function timer(string $name = NULL): float
  {
    if (!self::enabled()) return 0;

    return \Tracy\Debugger::timer($name);
  }

  /**
   * @tracySkipLocation
   */
  public static function barDump($var, string $title = NULL, array $options = [])
  {
    if (!self::enabled()) return null;

    return \Tracy\Debugger::barDump($var, $title, $options);
  }

  public static function log($message, string $priority = ILogger::INFO)
  {
    if (!self::enabled()) return null;

    return \Tracy\Debugger::log($message, $priority);
  }

  public static function fireLog($message)
  {
    if (!self::enabled()) return null;

    return \Tracy\Debugger::fireLog($message);
  }

  public static function profile(string $prefix, string $label = '', ?float $startTime = null, ?int $startMemory = null): void
  {
    if (!self::enabled()) return;

    if (!Profiler::exists($prefix)) {
      if ($startTime === null)
        $startTime = microtime(true);
      if ($startMemory === null)
        $startMemory = memory_get_usage();
      Profiler::getInstance($prefix)->setStart($startTime, $startMemory);
    }

    if ($label)
      Profiler::getInstance($prefix)->mark($label);
  }

  public static function registerPanel(\n3tDebug\Panel $panel) {
    if (self::$plugin)
      self::$plugin->registerPanel($panel);
  }

  public static function getCounters()
  {
    return self::$counters;
  }

  public static function counter(string $label)
  {
    if (!self::enabled()) return;

    if (isset(self::$counters[$label]))
      self::$counters[$label]++;
    else
      self::$counters[$label] = 1;
  }

  /**
   * @tracySkipLocation
   */
  public static function callStack(?string $label = null)
  {
    self::barDump(debug_backtrace(), $label);
  }

}
