n3t Debug
=========

![Version](https://img.shields.io/badge/Latest%20version-5.0.3-informational)
![Release date](https://img.shields.io/badge/Release%20date-2024--08--27-informational)

[![Joomla! CMS](https://img.shields.io/badge/Joomla!%20CMS-V3.10-green)][JOOMLA]
[![Joomla! CMS](https://img.shields.io/badge/Joomla!%20CMS-V4.x-green)][JOOMLA]
[![PHP](https://img.shields.io/badge/PHP-V7.2-green)][PHP]
[![Documentation Status](https://readthedocs.org/projects/n3t-debug/badge/?version=latest)](http://n3tdebug.docs.n3t.cz/en/latest/?badge=latest)

[Nette Tracy][TRACY] implementation for [Joomla! CMS][JOOMLA].

Install instructions
----------------------------
 * install package using Joomla! installer
 * enable plugin
 * set your IP address in plugin settings

[JOOMLA]: https://www.joomla.org
[PHP]: https://www.php.net
[TRACY]: https://tracy.nette.org
