<?php
/**
 * @package n3t Debug
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2016-2024 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Plugin\CMSPlugin;
use Joomla\Utilities\IpHelper;
use Tracy\Debugger;
use Joomla\CMS\Version;

class plgSystemN3tDebug extends CMSPlugin
{

  private $panels = [];
  private $enabled = false;
  /**
   * @var callable
   * @since 5.0.3
   */
  private $joomlaExceptionHandler = ['\Joomla\CMS\Exception\ExceptionHandler', 'handleException'];
  /**
   * @var array
   * @since 5.0.3
   */
  private $joomlaErrorHandler = null;

  private function getIp(): string
  {
    $ip = IpHelper::getIp();

    // MS Azure cloud servers contains port in IP address
    if (preg_match('~^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}:[0-9]+$~', $ip)) {
      $ip = preg_replace('~^([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}):[0-9]+$~', '$1', $ip);
    }

    return $ip;
  }

  public function __construct(&$subject, $config = array())
  {
    parent::__construct($subject, $config);

    $app = Factory::getApplication();
    if ($app->isClient('site') && !$this->params->get('enabled_site', '0'))
      return;

    if ($app->isClient('administrator') && !$this->params->get('enabled_admin', '0'))
      return;

    $ipList = preg_split('/\s*\n\s*/', $this->params->get('ip_list', ''));
    if (!IpHelper::IPinList($this->getIp(), $ipList))
      return;

    $this->enabled = true;

    if (Version::MAJOR_VERSION === 3)
      Factory::getConfig()->set('gzip', false);
    else
      $app->getConfig()->set('gzip', false);

    ob_start();
    ob_implicit_flush(false);
  }

  private function panelEnabled(string $name): bool
  {
    return $this->params->get('panel_' . strtolower($name) , '1') == 1;
  }

  private function addPanel(string $name)
  {
    if ($this->panelEnabled($name)) {
      $class = '\\n3tDebug\\Panel\\'.$name;
      $panel = new $class($this->params);
      $this->panels[$name] = $panel;
      Debugger::getBar()->addPanel($panel);
    }
  }

  public function registerPanel(\n3tDebug\Panel $panel)
  {
    if (!isset($this->panels[get_class($panel)])) {
      $this->panels[get_class($panel)] = $panel;
      Debugger::getBar()->addPanel($panel);
    }
  }

  public function onAfterInitialise()
  {
    if (Version::MAJOR_VERSION === 3)
      JLoader::registerNamespace('n3tDebug', __DIR__ . DIRECTORY_SEPARATOR);
    else
      JLoader::registerNamespace('n3tDebug', __DIR__ . DIRECTORY_SEPARATOR . 'n3tDebug');
    JLoader::register('n3tDebug', __DIR__ . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'n3tDebug.php');

    if (!$this->enabled)
      return;

    jimport('tracy.tracy');
    $paths = [
      Factory::getConfig()->get('tmp_path'),
      session_save_path(),
			ini_get('upload_tmp_dir'),
      sys_get_temp_dir(),
    ];
    $tmpPath = null;
    foreach($paths as $path) {
      if (@is_dir($path) && @is_writable($path)) {
        $tmpPath = $path;
        break;
      }
    }
    if ($tmpPath)
      $sessionStorage = new \Tracy\FileSession($tmpPath);
    else
      $sessionStorage = new \Tracy\NativeSession();
    Debugger::setSessionStorage($sessionStorage);

    set_exception_handler(null);
    Debugger::enable(Debugger::DEVELOPMENT);
    set_exception_handler([$this, 'exceptionHandler']);
    if (Version::MAJOR_VERSION === 3) {
      $this->joomlaErrorHandler = JError::getErrorHandling(E_ERROR);
      JError::setErrorHandling(E_ERROR, 'callback', [$this, 'exceptionHandler']);
    }

    Debugger::$dumpTheme = $this->params->get('dump_theme', 'light');
    Debugger::$maxLength = (int)$this->params->get('dump_max_length', 150);
    Debugger::getBlueScreen()->maxLength = (int)$this->params->get('dump_max_length', 150);
    Debugger::$maxDepth = (int)$this->params->get('dump_max_depth', 15);
    Debugger::getBlueScreen()->maxDepth = (int)$this->params->get('dump_max_depth', 15);
    $keysToHide = (array)$this->params->get('dump_keys_to_hide', []);
    array_walk($keysToHide, function (&$value) {
      $value = trim($value->name);
    });
    $keysToHide = array_filter($keysToHide);
    $keysToHide = array_values($keysToHide);
    $keysToHide = array_unique($keysToHide);
    Debugger::$keysToHide = $keysToHide;
    $keysToHide = array_merge($keysToHide, Debugger::getBlueScreen()->keysToHide);
    $keysToHide = array_unique($keysToHide);
    Debugger::getBlueScreen()->keysToHide = $keysToHide;
    Debugger::$showLocation = true;
    Debugger::$scream = !!$this->params->get('scream', false);
    if ($this->params->get('strict_mode', false)) {
      Debugger::$strictMode = E_ALL;
      foreach ($this->params->get('strict_mode_ignore_levels', [E_DEPRECATED, E_USER_DEPRECATED]) as $mode)
        Debugger::$strictMode &= ~(int)$mode;
    }

    $errorReporting = E_ALL;
    foreach ($this->params->get('ignore_levels', [0]) as $mode)
      $errorReporting &= ~(int)$mode;
    error_reporting($errorReporting);

    $this->addPanel('JInfo');
    $this->addPanel('JProfile');
    $this->addPanel('JUser');
    $this->addPanel('JLanguage');
    $this->addPanel('JLog');
    $this->addPanel('JSession');
    $this->addPanel('JMail');
    $this->addPanel('JScripts');
    $this->addPanel('JStyleSheets');
    $this->addPanel('JDatabase');

    if (
         $this->panelEnabled('Server')
      || $this->panelEnabled('Request')
      || $this->panelEnabled('Get')
      || $this->panelEnabled('Post')
      || $this->panelEnabled('Cookie')
      || $this->panelEnabled('Session')
    ) {
      $this->addPanel('PHP');
    }
    $this->addPanel('Server');
    $this->addPanel('Request');
    $this->addPanel('Get');
    $this->addPanel('Post');
    $this->addPanel('Cookie');
    $this->addPanel('Session');

    $this->addPanel('Counters');

    $panels = Factory::getApplication()->triggerEvent('onN3tDebugAddPanel');

    foreach ($panels as $panel)
      if (is_object($panel) && $panel instanceof \n3tDebug\Panel)
        $this->registerPanel($panel);

    n3tDebug::setPlugin($this);
    global $startTime, $startMem;
    n3tDebug::profile('Joomla', '', $startTime, $startMem);
    n3tDebug::profile('Joomla', 'onAfterInitialise');
  }

  public function exceptionHandler(\Throwable $exception): void
  {
    if (!!$this->params->get('ignore_404', true)) {
      if (Version::MAJOR_VERSION === 3) {
        $is404 = $this->joomlaErrorHandler
          && $this->joomlaErrorHandler['mode'] == 'callback'
          && is_callable($this->joomlaErrorHandler['options'])
          && $exception instanceof \JException
          && $exception->getCode() == 404;
      } else {
        $is404 = is_callable($this->joomlaExceptionHandler)
          && $exception instanceof \Joomla\CMS\Router\Exception\RouteNotFoundException;
      }
    } else {
      $is404 = false;
    }

    if ($is404) {
      if (Version::MAJOR_VERSION === 3) {
        call_user_func($this->joomlaErrorHandler['options'], $exception);
      } else {
        call_user_func($this->joomlaExceptionHandler, $exception);
      }
    } else {
      Debugger::exceptionHandler($exception);
    }
  }

  public function onAfterRespond()
  {
    n3tDebug::profile('Joomla', 'onAfterRespond');

    if (!$this->enabled)
      return;

    foreach ($this->panels as $panel)
      $panel->collectData();

    Debugger::shutdownHandler();
    Debugger::$showBar = false;
  }

  public function onError($error, $app)
  {
    if ($this->enabled)
      throw $error;
  }

  public function onAfterRoute()
  {
    n3tDebug::profile('Joomla', 'onAfterRoute');
  }

  public function onAfterDispatch()
  {
    n3tDebug::profile('Joomla', 'onAfterDispatch');
  }

  public function onBeforeRender()
  {
    n3tDebug::profile('Joomla', 'onBeforeRender');
  }

  public function onAfterRender()
  {
    n3tDebug::profile('Joomla', 'onAfterRender');
  }

  public function onBeforeCompileHead()
  {
    n3tDebug::profile('Joomla', 'onBeforeCompileHead');
  }

}
